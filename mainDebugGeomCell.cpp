#include <CommonFile/geom/StaticGeom.h>
#include <CommonFile/MakeMesh.h>

USE_PRJ_NAMESPACE

void debugTwoSphereMesh()
{
  ObjMesh m;
  MakeMesh::makeTwoSphereMesh2D(m,1,0.5,1,64);
  m.writeVTK("./twoSphere2DA.vtk",true);
  MakeMesh::makeTwoSphereMesh2D(m,1,1,1,64);
  m.writeVTK("./twoSphere2DB.vtk",true);
  MakeMesh::makeTwoSphereMesh2D(m,0.5,1,1,64);
  m.writeVTK("./twoSphere2DC.vtk",true);

  MakeMesh::makeTwoSphereMesh3D(m,1,0.5,1,64);
  m.writeVTK("./twoSphere3DA.vtk",true);
  MakeMesh::makeTwoSphereMesh3D(m,1,1,1,64);
  m.writeVTK("./twoSphere3DB.vtk",true);
  MakeMesh::makeTwoSphereMesh3D(m,0.5,1,1,64);
  m.writeVTK("./twoSphere3DC.vtk",true);
}
void debugThreeSphereMesh()
{
  ObjMesh m;
  MakeMesh::makeThreeSphereMesh2D(m,RandEngine::randR01(),RandEngine::randR01(),RandEngine::randR01(),Vec2(0,0),Vec2(4,0),Vec2(2,4),64);
  m.writeVTK("./threeSphere2DA.vtk",true);
  MakeMesh::makeThreeSphereMesh2D(m,RandEngine::randR01(),RandEngine::randR01(),RandEngine::randR01(),Vec2(0,0),Vec2(2,4),Vec2(4,0),64);
  m.writeVTK("./threeSphere2DB.vtk",true);
  MakeMesh::makeThreeSphereMesh2D(m,RandEngine::randR01()+2,RandEngine::randR01(),RandEngine::randR01(),Vec2(0,0),Vec2(2,4),Vec2(4,0),64);
  m.writeVTK("./threeSphere2DC.vtk",true);

  MakeMesh::makeThreeSphereMesh3D(m,RandEngine::randR01(),RandEngine::randR01(),RandEngine::randR01(),Vec2(0,0),Vec2(4,0),Vec2(2,4),64);
  m.writeVTK("./threeSphere3DA.vtk",true);
  MakeMesh::makeThreeSphereMesh3D(m,RandEngine::randR01(),RandEngine::randR01(),RandEngine::randR01(),Vec2(0,0),Vec2(2,4),Vec2(4,0),64);
  m.writeVTK("./threeSphere3DB.vtk",true);
  MakeMesh::makeThreeSphereMesh3D(m,RandEngine::randR01()+2,RandEngine::randR01(),RandEngine::randR01(),Vec2(0,0),Vec2(2,4),Vec2(4,0),64);
  m.writeVTK("./threeSphere3DC.vtk",true);
}
int main(int argc,char** argv)
{
  debugTwoSphereMesh();
  debugThreeSphereMesh();
  StaticGeom::debugRayVertexQuery(false,false,false,false,false,false,false,false,false);
  return 0;
}
