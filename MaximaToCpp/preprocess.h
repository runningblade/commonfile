#ifndef PREPROCESS_H
#define PREPROCESS_H

#include <stdio.h>

static void preprocess(FILE* in,FILE* out)
{
  char c;
  while(!feof(in)) {
    c=fgetc(in);
    if(c == '<' || c == '>')
      break;
    if(c != '&' && c != '\t' && c != '\n')
      fputc(c,out);
  }
}

enum treetype {
  program,
  variables,
  assigns,
  assignExpr,
  results,
  exprs,
  expr,
  term,
  factor,
  bracket,
  func,
  param,
  diff,
  adouble,
  aint,
  avar,
  leaf,
};

typedef struct tree {
  enum treetype type;
  void* a;
  void* b;
  void* c;
  int ta;
  int tb;
  int tc;
} tree;

typedef struct YYST {
  int yyl;
  void* p;
} yyst;

#endif
