#ifndef ANALYSE_TREE_H
#define ANALYSE_TREE_H

#include <iostream>
#include <sstream>
#include <vector>
#include <map>
#include <set>
#include <string>
#include <algorithm>
#include <assert.h>
#include <cmath>
#include "preprocess.h"

//the program

using namespace std;

class Tree
{
public:
  struct Variable {
    Variable() {}
    Variable(const string& t,const string& n,int r,int c):type(t),name(n),row(r),col(c) {}
    int nr() const {
      if(row == -1 && col == -1)
        return 1;
      else if(row != -1 && col != -1)
        return row*col;
      else if(row != -1)
        return row;
      else return col;
    }
    std::string entry(int off) const {
      ostringstream ossTmp;
      if(row == -1 && col == -1)
        ossTmp << name;
      else if(row != -1 && col != -1)
        ossTmp << name << "(" << (off/col) << "," << (off%col) << ")";
      else ossTmp << name << "[" << off << "]";
      return ossTmp.str();
    }
    string type;
    string name;
    int row,col;
  };
  Tree(tree* r):treeRoot(r) {
    setScalar("scalarD");
  }
  void setScalar(const std::string& type) {
    scalar=type;
  }
  void addInput(const Variable& v) {
    inputIndex[v.name]=v;
  }
  void addOutput(const Variable& v) {
    outputIndex.push_back(v);
  }
  void parse(ostream& os) {
    input.clear();
    tempVars.clear();
    oss.clear();
    oss.str("");
    index=0;

    parseProgram(treeRoot);

    os << "//input" << std::endl;
    for(set<string>::const_iterator beg=input.begin(),end=input.end(); beg!=end; beg++) {
      map<string,Variable>::const_iterator iter=inputIndex.find(*beg);
      if(iter == inputIndex.end())
        os << scalar << " " << *beg << ";" << std::endl;
      else os << iter->second.type << " " << *beg << ";" << std::endl;
    }
    os << std::endl;

    os << "//temp" << std::endl;
    for(vector<string>::const_iterator beg=tempVars.begin(),end=tempVars.end(); beg!=end; beg++) {
      os << scalar << " " << *beg << ";" << std::endl;
    }
    os << std::endl;

    os << oss.str();
  }
protected:
  void parseProgram(tree* t) {
    if(t->a != NULL) {
      parseVariables((tree*)(t->a));
      parseAssigns((tree*)(t->b));
    }
    parseResults((tree*)(t->c));
  }
  void parseVariables(tree* t) {
    assert(t->type==variables);
    tree* leaf=(tree*)(t->a);
    tempVars.push_back(rename(std::string((char*)(leaf->a))));
    if(t->b)
      parseVariables((tree*)(t->b));
  }
  void parseAssigns(tree* t) {
    assert(t->type==assigns);
    parseAssign((tree*)(t->a));
    if(t->b)
      parseAssigns((tree*)(t->b));
  }
  void parseAssign(tree* t) {
    assert(t->type==assignExpr);
    std::string name=rename((char*)(((tree*)(t->a))->a));
    oss << name << "=";
    parseExpr((tree*)(t->b),oss);
    oss << ";" << std::endl;
  }
  void parseResults(tree* t) {
    assert(t->type==results);
    parseResult((tree*)(t->a));
    if(t->b)
      parseResults((tree*)(t->b));
  }
  void parseResult(tree* t) {
    assert(t->type==expr);
    oss << findOutputName();
    oss << "=";
    parseExpr(t,oss);
    oss << ";" << std::endl;
    index++;
  }
  void parseExprs(tree* t,ostream& os) {
    assert(t->type==exprs);
    parseExpr((tree*)(t->a),os);
    if(t->b) {
      os << ',';
      parseExprs((tree*)(t->b),os);
    }
  }
  void parseExprs(tree* t,std::vector<string>& os) {
    assert(t->type==exprs);
    ostringstream tmp;
    parseExpr((tree*)(t->a),tmp);
    os.push_back(tmp.str());
    if(t->b)
      parseExprs((tree*)(t->b),os);
  }
  void parseExpr(tree* t,ostream& os) {
    assert(t->type==expr);
    if(t->b == 0) {
      parseTerm((tree*)(t->a),os);
    } else {
      std::string op=(char*)(((tree*)(t->a))->a);
      if(t->c == 0) {
        os << op;
        parseExpr((tree*)(t->b),os);
      } else {
        parseTerm((tree*)(t->b),os);
        os << op;
        parseExpr((tree*)(t->c),os);
      }
    }
  }
  void parseTerm(tree* t,ostream& os) {
    assert(t->type==term);
    if(t->b == 0)
      parseFactor((tree*)(t->a),os);
    else {
      std::string op=(char*)(((tree*)(t->a))->a);
      parseFactor((tree*)(t->b),os);
      os << op;
      parseTerm((tree*)(t->c),os);
    }
  }
  void parseFactor(tree* t,ostream& os) {
    assert(t->type==factor);
    if(t->b == 0)
      parseFactor2((tree*)(t->a),os);
    else {
      os << "pow(";
      parseFactor2((tree*)(t->b),os);
      os << ",";
      parseFactor((tree*)(t->c),os);
      os << ")";
    }
  }
  void parseFactor2(tree* t,ostream& os) {
    int diffOrder;
    std::vector<string> ossTmp;
    switch(t->type) {
    case bracket:
      os << "(";
      parseExpr((tree*)(t->a),os);
      os << ")";
      break;
    case func:
      parseExprs((tree*)(t->b),ossTmp);
      os << findEntry((char*)(((tree*)(t->a))->a),ossTmp);
      break;
    case diff:
      os << "D";
      diffOrder=atoi((char*)(((tree*)(t->c))->a));
      for(int i=0; i<diffOrder; i++)
        os << (char*)(((tree*)(t->b))->a);
      os << "(";
      parseFactor2((tree*)(t->a),os);
      os << ")";
      break;
    case adouble:
    case aint:
      os << (char*)(((tree*)(t->a))->a);
      break;
    case avar:
      os << rename((char*)(((tree*)(t->a))->a));
      break;
    case param:
      os << (char*)(((tree*)(t->a))->a);
      input.insert((char*)(((tree*)(t->a))->a));
      break;
    default:
      assert(false);
    };
  }
  std::string findOutputName() const {
    int curr=0;
    for(size_t i=0; i<outputIndex.size(); i++) {
      if(curr+outputIndex[i].nr() > index)
        return outputIndex[i].entry(index-curr);
      curr+=outputIndex[i].nr();
    }
    ostringstream ossTmp;
    ossTmp << "out[" << index << "]";
    return ossTmp.str();
  }
  std::string findEntry(const std::string& name,const std::vector<string>& params) {
    ostringstream ossTmp;
    if(inputIndex.find(name) == inputIndex.end()) {
      ossTmp << name << "(";
      for(size_t i=0; i<params.size(); i++) {
        ossTmp << params[i];
        if(i < params.size()-1)ossTmp << ",";
      }
      ossTmp << ")";
    } else {
      ossTmp << name << (params.size() == 1 ? "[" : "(");
      for(size_t i=0; i<params.size(); i++) {
        input.insert(name);
        ossTmp << minusOne(params[i]);
        if(i < params.size()-1)ossTmp << ",";
      }
      ossTmp << (params.size() == 1 ? "]" : ")");
    }
    return ossTmp.str();
  }
  std::string minusOne(const std::string& expr) const {
    int val=atoi(expr.c_str());
    bool valid=true;
    {
      ostringstream ossTmp;
      ossTmp << val;
      valid=ossTmp.str() == expr;
    }
    ostringstream ossTmp;
    if(valid) {
      ossTmp << (val-1);
      return ossTmp.str();
    } else {
      ossTmp << expr << "-1";
      return ossTmp.str();
    }
  }
  static std::string rename(const std::string& name) {
    int val;
    sscanf(name.c_str()+1,"%d",&val);
    ostringstream ossTmp;
    ossTmp << "tt" << val;
    return ossTmp.str();
  }
protected:
  string scalar;
  map<string,Variable> inputIndex;
  vector<Variable> outputIndex;
  set<string> input;
  vector<string> tempVars;
  ostringstream oss;
  int index;
  tree* treeRoot;
};

#endif
