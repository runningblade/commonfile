#include "preprocess.h"
#include "analyseTree.h"

#include <stdio.h>
#include <stdlib.h>
#include <fstream>

extern "C" {
  int yyparse();
  extern FILE *yyin;
  extern tree* root;
}

Tree::Variable readVar(char* c)
{
  std::string str(c);
  for(int i=0; i<(int)str.size(); i++)
    if(str[i] == ',')
      str[i]=' ';

  Tree::Variable v;
  std::istringstream is(str);
  is >> v.type >> v.name >> v.row >> v.col;
  return v;
}

int main(int argc,char** argv)
{
  if(argc < 3) {
    std::cout << "Maxima to Cpp Translator Usage:" << std::endl;
    std::cout << "MaximaToCpp [InputFilePath] [OutputFilePath] -I <InputMatrixVectorOptions> -O <OutputMatrixVectorOptions> -S <ScalarOptions>" << std::endl;
    std::cout << "Format for Input/OutputMatrixVectorOptions: Type,NameString,NumberOfRows,NumberOfColumns" << std::endl;
    std::cout << "For example, your output is a Vector3, which has name: position, then you type: -I Vector3,position,3,1" << std::endl;
    std::cout << "By default, all scalars are replaced with string \"scalar\", you can change that to float or double by typing: -S double" << endl;
    exit(EXIT_SUCCESS);
  }

  //preprocess
  {
    FILE* fi=fopen(argv[1],"r");
    FILE* fo=fopen("./temp","w");
    if(!fi || !fo) {
      std::cout << "Input/Output file not found!" << std::endl;
      exit(EXIT_FAILURE);
    }
    preprocess(fi,fo);
    fclose(fi);
    fclose(fo);
  }

  //parse
  {
    yyin=fopen("./temp","r");
    yyparse();
  }

  //analyse
  {
    std::ofstream os(argv[2]);
    Tree t(root);
    for(int i=3; i<argc; i++) {
      if(std::string("-I") == argv[i])
        t.addInput(readVar(argv[i+1]));
      else if(std::string("-O") == argv[i])
        t.addOutput(readVar(argv[i+1]));
      else if(std::string("-S") == argv[i])
        t.setScalar(argv[i+1]);
    }
    t.parse(os);
  }
  exit(EXIT_SUCCESS);
}
