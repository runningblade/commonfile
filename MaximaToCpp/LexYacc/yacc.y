%{
	#define YYSTYPE void*
	#include "preprocess.h"
    #include <stdio.h>
    int yylex(void);
    void yyerror(char *);
	void* createRoot(void* a,void* b,void* c);
	void* node2(enum treetype name,void* a,int ta);
	void* node3(enum treetype name,void* a,void* b,int ta,int tb);
	void* node4(enum treetype name,void* a,void* b,void* c,int ta,int tb,int tc);
%}

%token BLOCK
%token DIFF
%token FUNC_OR_NAME

%token CONST_DOUBLE
%token CONST_INT

%token SEPARATOR
%token VAR
%token IS

%token ADD_OP
%token SUB_OP
%token MUL_OP
%token DIV_OP
%token POW_OP
%token COMMA

%token LB
%token RB
%token LBV
%token RBV

%%

program:
        BLOCK LB LBV variables RBV assigns LBV results RBV RB	{createRoot($4,$6,$8);}
        | LBV results RBV										{createRoot(0,0,$2);}
		;

variables:
        VAR COMMA variables										{$$=node3(variables,$1,$3,1,0);}
        | VAR													{$$=node2(variables,$1,1);}
        ;
		
assigns:
		COMMA assign assigns									{$$=node3(assigns,$2,$3,0,0);}
		| COMMA assign COMMA									{$$=node2(assigns,$2,0);}
		;
		
assign:
		VAR IS expr												{$$=node3(assignExpr,$1,$3,1,0);}
		;

results:
		LBV expr RBV COMMA results								{$$=node3(results,$2,$5,0,0);}
		| expr COMMA results									{$$=node3(results,$1,$3,0,0);}
		| LBV expr RBV											{$$=node2(results,$2,0);}
		| expr													{$$=node2(results,$1,0);}
		;

exprs:	
		expr COMMA exprs										{$$=node3(exprs,$1,$3,0,0);}
		| expr													{$$=node2(exprs,$1,0);}
		;
		
expr:	
		ADD_OP expr												{$$=node3(expr,$1,$2,1,0);}
		| SUB_OP expr											{$$=node3(expr,$1,$2,1,0);}
		| term ADD_OP expr										{$$=node4(expr,$2,$1,$3,1,0,0);}
		| term SUB_OP expr										{$$=node4(expr,$2,$1,$3,1,0,0);}
		| term													{$$=node2(expr,$1,0);}
		;
		
term:	
		factor MUL_OP term										{$$=node4(term,$2,$1,$3,1,0,0);}
		| factor DIV_OP term									{$$=node4(term,$2,$1,$3,1,0,0);}
		| factor												{$$=node2(term,$1,0);}
		;
		
factor:
		factor2 POW_OP factor									{$$=node4(factor,$2,$1,$3,1,0,0);}
		| factor2												{$$=node2(factor,$1,0);}
		;
		
factor2:	
		LB expr RB												{$$=node2(bracket,$2,0);}
		| FUNC_OR_NAME LB exprs RB								{$$=node3(func,$1,$3,1,0);}
		| FUNC_OR_NAME											{$$=node2(param,$1,1);}
		| DIFF LB factor2 COMMA FUNC_OR_NAME COMMA CONST_INT RB	{$$=node4(diff,$3,$5,$7,1,1,1);}
		| CONST_DOUBLE											{$$=node2(adouble,$1,1);}
		| CONST_INT												{$$=node2(aint,$1,1);}
		| VAR													{$$=node2(avar,$1,1);}
		;
%%

void yyerror(char *s) {
    fprintf(stderr, "%s\n", s);
}

tree* root=0;
void* createRoot(void* a,void* b,void* c)
{
	tree* ret=(tree*)malloc(sizeof(tree));
	ret->type=program;
	ret->a=a;
	ret->b=b;
	ret->c=c;
	ret->ta=0;
	ret->tb=0;
	ret->tc=0;
	root=ret;
	return ret;
}
void* node2(enum treetype name,void* a,int ta)
{
	tree* ret=(tree*)malloc(sizeof(tree));
	ret->type=name;
	ret->a=a;
	ret->b=0;
	ret->c=0;
	ret->ta=ta;
	ret->tb=-1;
	ret->tc=-1;
	return ret;
}
void* node3(enum treetype name,void* a,void* b,int ta,int tb)
{
	tree* ret=(tree*)malloc(sizeof(tree));
	ret->type=name;
	ret->a=a;
	ret->b=b;
	ret->c=0;
	ret->ta=ta;
	ret->tb=tb;
	ret->tc=-1;
	return ret;
}
void* node4(enum treetype name,void* a,void* b,void* c,int ta,int tb,int tc)
{
	tree* ret=(tree*)malloc(sizeof(tree));
	ret->type=name;
	ret->a=a;
	ret->b=b;
	ret->c=c;
	ret->ta=ta;
	ret->tb=tb;
	ret->tc=tc;
	return ret;
}