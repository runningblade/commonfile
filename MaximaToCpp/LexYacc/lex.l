%{
	#define YYSTYPE void*
	#include "preprocess.h"
	#include "yacc.tab.h"
	void* nodeLeaf(char* yytext,int yylen);
%}

%%

"block"			{yylval=nodeLeaf(yytext,yyleng);printf("BLOCK ");return(BLOCK);}
"'diff"			{yylval=nodeLeaf(yytext,yyleng);printf("DIFF ");return(DIFF);}
[A-Za-z_]+[A-Za-z_0-9]*		{yylval=nodeLeaf(yytext,yyleng);printf("FUNC_OR_NAME ");return(FUNC_OR_NAME);}

[0-9]*\.[0-9]+([eE][-+]?[0-9]+)?	{yylval=nodeLeaf(yytext,yyleng);printf("CONST_DOUBLE(%s) ",yytext);return(CONST_DOUBLE);}
[0-9]+			{yylval=nodeLeaf(yytext,yyleng);printf("CONST_INT(%s) ",yytext);return(CONST_INT);}

[&\t]+$									;
%[0-9]+   		{yylval=nodeLeaf(yytext,yyleng);printf("VAR(%s) ",yytext);return(VAR);}
":"   			{yylval=nodeLeaf(yytext,yyleng);printf("IS ");return(IS);}

"+"				{yylval=nodeLeaf(yytext,yyleng);printf("ADD_OP ");return(ADD_OP);}
"-"				{yylval=nodeLeaf(yytext,yyleng);printf("SUB_OP ");return(SUB_OP);}
"*"				{yylval=nodeLeaf(yytext,yyleng);printf("MUL_OP ");return(MUL_OP);}
"/"				{yylval=nodeLeaf(yytext,yyleng);printf("DIV_OP ");return(DIV_OP);}
"**"			{yylval=nodeLeaf(yytext,yyleng);printf("POW_OP ");return(POW_OP);}
","				{yylval=nodeLeaf(yytext,yyleng);printf("COMMA ");return(COMMA);}

"("				{yylval=nodeLeaf(yytext,yyleng);printf("LB ");return(LB);}
")"				{yylval=nodeLeaf(yytext,yyleng);printf("RB ");return(RB);}
"["				{yylval=nodeLeaf(yytext,yyleng);printf("LBV ");return(LBV);}
"]"				{yylval=nodeLeaf(yytext,yyleng);printf("RBV ");return(RBV);}

%%

int yywrap()
{
	return 1;
}

void* nodeLeaf(char* yytext,int yylen)
{
	tree* ret=(tree*)malloc(sizeof(tree));
	ret->type=leaf;
	ret->a=malloc(yylen+1);
	memcpy(ret->a,yytext,yylen);
	((char*)(ret->a))[yylen]=0;
	ret->b=0;
	ret->c=0;
	ret->ta=1;
	ret->tb=-1;
	ret->tc=-1;
	return ret;
}