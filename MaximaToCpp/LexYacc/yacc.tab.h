#ifndef YYSTYPE
#define YYSTYPE int
#endif
#define	BLOCK	257
#define	DIFF	258
#define	FUNC_OR_NAME	259
#define	CONST_DOUBLE	260
#define	CONST_INT	261
#define	SEPARATOR	262
#define	VAR	263
#define	IS	264
#define	ADD_OP	265
#define	SUB_OP	266
#define	MUL_OP	267
#define	DIV_OP	268
#define	POW_OP	269
#define	COMMA	270
#define	LB	271
#define	RB	272
#define	LBV	273
#define	RBV	274


extern YYSTYPE yylval;
