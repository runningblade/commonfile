
/*  A Bison parser, made from yacc.y
    by GNU Bison version 1.28  */

#define YYBISON 1  /* Identify Bison output.  */

#define	BLOCK	257
#define	DIFF	258
#define	FUNC_OR_NAME	259
#define	CONST_DOUBLE	260
#define	CONST_INT	261
#define	SEPARATOR	262
#define	VAR	263
#define	IS	264
#define	ADD_OP	265
#define	SUB_OP	266
#define	MUL_OP	267
#define	DIV_OP	268
#define	POW_OP	269
#define	COMMA	270
#define	LB	271
#define	RB	272
#define	LBV	273
#define	RBV	274

#line 1 "yacc.y"

#define YYSTYPE void*
#include "preprocess.h"
#include <stdio.h>
int yylex(void);
void yyerror(char *);
void* createRoot(void* a,void* b,void* c);
void* node2(enum treetype name,void* a,int ta);
void* node3(enum treetype name,void* a,void* b,int ta,int tb);
void* node4(enum treetype name,void* a,void* b,void* c,int ta,int tb,int tc);
#ifndef YYSTYPE
#define YYSTYPE int
#endif
#include <stdio.h>

#ifndef __cplusplus
#ifndef __STDC__
#define const
#endif
#endif



#define	YYFINAL		72
#define	YYFLAG		-32768
#define	YYNTBASE	21

#define YYTRANSLATE(x) ((unsigned)(x) <= 274 ? yytranslate[x] : 31)

static const char yytranslate[] = {     0,
                                        2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                        2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                        2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                        2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                        2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                        2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                        2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                        2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                        2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                        2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                        2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                        2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                        2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                        2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                        2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                        2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                        2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                        2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                        2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                        2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                        2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                        2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                        2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                        2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                        2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
                                        2,     2,     2,     2,     2,     1,     3,     4,     5,     6,
                                        7,     8,     9,    10,    11,    12,    13,    14,    15,    16,
                                        17,    18,    19,    20
                                  };

#if YYDEBUG != 0
static const short yyprhs[] = {     0,
                                    0,    11,    15,    19,    21,    25,    29,    33,    39,    43,
                                    47,    49,    53,    55,    58,    61,    65,    69,    71,    75,
                                    79,    81,    85,    87,    91,    96,    98,   107,   109,   111
                              };

static const short yyrhs[] = {     3,
                                   17,    19,    22,    20,    23,    19,    25,    20,    18,     0,
                                   19,    25,    20,     0,     9,    16,    22,     0,     9,     0,
                                   16,    24,    23,     0,    16,    24,    16,     0,     9,    10,
                                   27,     0,    19,    27,    20,    16,    25,     0,    27,    16,
                                   25,     0,    19,    27,    20,     0,    27,     0,    27,    16,
                                   26,     0,    27,     0,    11,    27,     0,    12,    27,     0,
                                   28,    11,    27,     0,    28,    12,    27,     0,    28,     0,
                                   29,    13,    28,     0,    29,    14,    28,     0,    29,     0,
                                   30,    15,    29,     0,    30,     0,    17,    27,    18,     0,
                                   5,    17,    26,    18,     0,     5,     0,     4,    17,    30,
                                   16,     5,    16,     7,    18,     0,     6,     0,     7,     0,
                                   9,     0
                             };

#endif

#if YYDEBUG != 0
static const short yyrline[] = { 0,
                                 38,    40,    43,    45,    48,    50,    53,    57,    59,    60,
                                 61,    64,    66,    69,    71,    72,    73,    74,    77,    79,
                                 80,    83,    85,    88,    90,    91,    92,    93,    94,    95
                               };
#endif


#if YYDEBUG != 0 || defined (YYERROR_VERBOSE)

static const char * const yytname[] = {   "$","error","$undefined.","BLOCK",
                                          "DIFF","FUNC_OR_NAME","CONST_DOUBLE","CONST_INT","SEPARATOR","VAR","IS","ADD_OP",
                                          "SUB_OP","MUL_OP","DIV_OP","POW_OP","COMMA","LB","RB","LBV","RBV","program",
                                          "variables","assigns","assign","results","exprs","expr","term","factor","factor2", NULL
                                      };
#endif

static const short yyr1[] = {     0,
                                  21,    21,    22,    22,    23,    23,    24,    25,    25,    25,
                                  25,    26,    26,    27,    27,    27,    27,    27,    28,    28,
                                  28,    29,    29,    30,    30,    30,    30,    30,    30,    30
                            };

static const short yyr2[] = {     0,
                                  10,     3,     3,     1,     3,     3,     3,     5,     3,     3,
                                  1,     3,     1,     2,     2,     3,     3,     1,     3,     3,
                                  1,     3,     1,     3,     4,     1,     8,     1,     1,     1
                            };

static const short yydefact[] = {     0,
                                      0,     0,     0,     0,    26,    28,    29,    30,     0,     0,
                                      0,     0,     0,    11,    18,    21,    23,     0,     0,     0,
                                      14,    15,     0,     0,     2,     0,     0,     0,     0,     0,
                                      0,     4,     0,     0,     0,    13,    24,    10,     9,    16,
                                      17,    19,    20,    22,     0,     0,     0,    25,     0,     0,
                                      3,     0,     0,     0,    12,     8,     0,     0,     0,     0,
                                      0,     6,     5,     0,     0,     7,     0,    27,     1,     0,
                                      0,     0
                                };

static const short yydefgoto[] = {    70,
                                      33,    53,    58,    13,    35,    14,    15,    16,    17
                                 };

static const short yypact[] = {     2,
                                    -11,     3,    -6,    12,    13,-32768,-32768,-32768,    27,    27,
                                    27,    27,     7,    19,     5,    10,    26,    33,    41,    27,
                                    -32768,-32768,    25,    29,-32768,     3,    27,    27,    41,    41,
                                    41,    35,    34,    37,    38,    39,-32768,    43,-32768,-32768,
                                    -32768,-32768,-32768,-32768,    33,    44,    52,-32768,    27,     3,
                                    -32768,    53,    42,    47,-32768,-32768,    54,    49,     3,    59,
                                    27,    53,-32768,    48,    51,-32768,    55,-32768,-32768,    67,
                                    70,-32768
                              };

static const short yypgoto[] = {-32768,
                                30,    14,-32768,   -22,    22,    -9,    -4,    45,    58
                               };


#define	YYLAST		77


static const short yytable[] = {    21,
                                    22,    23,    24,    39,     1,     3,     4,     5,     6,     7,
                                    36,     8,    18,     9,    10,    27,    28,    40,    41,    11,
                                    2,    12,    29,    30,    42,    43,    25,    56,    19,    20,
                                    4,     5,     6,     7,    26,     8,    64,     9,    10,    36,
                                    31,    32,    37,    11,     4,     5,     6,     7,    38,     8,
                                    45,    66,    47,    46,    49,    48,    54,    11,    50,    52,
                                    59,    57,    60,    61,    62,    65,    71,    67,    68,    72,
                                    55,    63,    69,     0,    51,    44,    34
                               };

static const short yycheck[] = {     9,
                                     10,    11,    12,    26,     3,    17,     4,     5,     6,     7,
                                     20,     9,    19,    11,    12,    11,    12,    27,    28,    17,
                                     19,    19,    13,    14,    29,    30,    20,    50,    17,    17,
                                     4,     5,     6,     7,    16,     9,    59,    11,    12,    49,
                                     15,     9,    18,    17,     4,     5,     6,     7,    20,     9,
                                     16,    61,    16,    20,    16,    18,     5,    17,    16,    16,
                                     19,     9,    16,    10,    16,     7,     0,    20,    18,     0,
                                     49,    58,    18,    -1,    45,    31,    19
                               };
/* -*-C-*-  Note some compilers choke on comments on `#line' lines.  */
#line 3 "bison.simple"

/* Skeleton output parser for bison,
   Copyright (C) 1984, 1989, 1990 Bob Corbett and Richard Stallman

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 1, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  */


#ifndef alloca
#ifdef __GNUC__
#define alloca __builtin_alloca
#else /* not GNU C.  */
#if (!defined (__STDC__) && defined (sparc)) || defined (__sparc__) || defined (__sparc)
#include <alloca.h>
#else /* not sparc */
#if (defined (MSDOS) || defined(WIN32)) && !defined (__TURBOC__)
#include <malloc.h>
#else /* not MSDOS, or __TURBOC__ */
#if defined(_AIX)
#include <malloc.h>
#pragma alloca
#endif /* not _AIX */
#endif /* not MSDOS, or __TURBOC__ */
#endif /* not sparc.  */
#endif /* not GNU C.  */
#endif /* alloca not defined.  */

/* This is the parser code that is written into each bison parser
  when the %semantic_parser declaration is not specified in the grammar.
  It was written by Richard Stallman by simplifying the hairy parser
  used when %semantic_parser is specified.  */

/* Note: there must be only one dollar sign in this file.
   It is replaced by the list of actions, each action
   as one case of the switch.  */

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		-2
#define YYEOF		0
#define YYACCEPT	return(0)
#define YYABORT 	return(1)
#define YYERROR		goto yyerrlab1
/* Like YYERROR except do call yyerror.
   This remains here temporarily to ease the
   transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */
#define YYFAIL		goto yyerrlab
#define YYRECOVERING()  (!!yyerrstatus)
#define YYBACKUP(token, value) \
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    { yychar = (token), yylval = (value);			\
      yychar1 = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    { yyerror ("syntax error: cannot back up"); YYERROR; }	\
while (0)

#define YYTERROR	1
#define YYERRCODE	256

#ifndef YYPURE
#define YYLEX		yylex()
#endif

#ifdef YYPURE
#ifdef YYLSP_NEEDED
#define YYLEX		yylex(&yylval, &yylloc)
#else
#define YYLEX		yylex(&yylval)
#endif
#endif

/* If nonreentrant, generate the variables here */

#ifndef YYPURE

int	yychar;			/*  the lookahead symbol		*/
YYSTYPE	yylval;			/*  the semantic value of the		*/
/*  lookahead symbol			*/

#ifdef YYLSP_NEEDED
YYLTYPE yylloc;			/*  location data for the lookahead	*/
/*  symbol				*/
#endif

int yynerrs;			/*  number of parse errors so far       */
#endif  /* not YYPURE */

#if YYDEBUG != 0
int yydebug;			/*  nonzero means print parse trace	*/
/* Since this is uninitialized, it does not stop multiple parsers
   from coexisting.  */
#endif

/*  YYINITDEPTH indicates the initial size of the parser's stacks	*/

#ifndef	YYINITDEPTH
#define YYINITDEPTH 200
#endif

/*  YYMAXDEPTH is the maximum size the stacks can grow to
    (effective only if the built-in stack extension method is used).  */

#if YYMAXDEPTH == 0
#undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
#define YYMAXDEPTH 10000
#endif

#if __GNUC__ > 1		/* GNU C and GNU C++ define this.  */
#define __yy_bcopy(FROM,TO,COUNT)	__builtin_memcpy(TO,FROM,COUNT)
#else				/* not GNU C or C++ */
#ifndef __cplusplus

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_bcopy (from, to, count)
char *from;
char *to;
int count;
{
    register char *f = from;
    register char *t = to;
    register int i = count;

    while (i-- > 0)
        *t++ = *f++;
}

#else /* __cplusplus */

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_bcopy (char *from, char *to, int count)
{
    register char *f = from;
    register char *t = to;
    register int i = count;

    while (i-- > 0)
        *t++ = *f++;
}

#endif
#endif


#line 169 "bison.simple"
int
yyparse()
{
    register int yystate;
    register int yyn;
    register short *yyssp;
    register YYSTYPE *yyvsp;
    int yyerrstatus;	/*  number of tokens to shift before error messages enabled */
    int yychar1;		/*  lookahead token as an internal (translated) token number */

    short	yyssa[YYINITDEPTH];	/*  the state stack			*/
    YYSTYPE yyvsa[YYINITDEPTH];	/*  the semantic value stack		*/

    short *yyss = yyssa;		/*  refer to the stacks thru separate pointers */
    YYSTYPE *yyvs = yyvsa;	/*  to allow yyoverflow to reallocate them elsewhere */

#ifdef YYLSP_NEEDED
    YYLTYPE yylsa[YYINITDEPTH];	/*  the location stack			*/
    YYLTYPE *yyls = yylsa;
    YYLTYPE *yylsp;

#define YYPOPSTACK   (yyvsp--, yyssp--, yylsp--)
#else
#define YYPOPSTACK   (yyvsp--, yyssp--)
#endif

    int yystacksize = YYINITDEPTH;

#ifdef YYPURE
    int yychar;
    YYSTYPE yylval;
    int yynerrs;
#ifdef YYLSP_NEEDED
    YYLTYPE yylloc;
#endif
#endif

    YYSTYPE yyval;		/*  the variable used to return		*/
    /*  semantic values from the action	*/
    /*  routines				*/

    int yylen;

#if YYDEBUG != 0
    if (yydebug)
        fprintf(stderr, "Starting parse\n");
#endif

    yystate = 0;
    yyerrstatus = 0;
    yynerrs = 0;
    yychar = YYEMPTY;		/* Cause a token to be read.  */

    /* Initialize stack pointers.
       Waste one element of value and location stack
       so that they stay on the same level as the state stack.
       The wasted elements are never initialized.  */

    yyssp = yyss - 1;
    yyvsp = yyvs;
#ifdef YYLSP_NEEDED
    yylsp = yyls;
#endif

    /* Push a new state, which is found in  yystate  .  */
    /* In all cases, when you get here, the value and location stacks
       have just been pushed. so pushing a state here evens the stacks.  */
yynewstate:

    *++yyssp = yystate;

    if (yyssp >= yyss + yystacksize - 1) {
        /* Give user a chance to reallocate the stack */
        /* Use copies of these so that the &'s don't force the real ones into memory. */
        YYSTYPE *yyvs1 = yyvs;
        short *yyss1 = yyss;
#ifdef YYLSP_NEEDED
        YYLTYPE *yyls1 = yyls;
#endif

        /* Get the current used size of the three stacks, in elements.  */
        int size = yyssp - yyss + 1;

#ifdef yyoverflow
        /* Each stack pointer address is followed by the size of
        the data in use in that stack, in bytes.  */
        yyoverflow("parser stack overflow",
                   &yyss1, size * sizeof (*yyssp),
                   &yyvs1, size * sizeof (*yyvsp),
#ifdef YYLSP_NEEDED
                   &yyls1, size * sizeof (*yylsp),
#endif
                   &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
#ifdef YYLSP_NEEDED
        yyls = yyls1;
#endif
#else /* no yyoverflow */
        /* Extend the stack our own way.  */
        if (yystacksize >= YYMAXDEPTH) {
            yyerror("parser stack overflow");
            return 2;
        }
        yystacksize *= 2;
        if (yystacksize > YYMAXDEPTH)
            yystacksize = YYMAXDEPTH;
        yyss = (short *) alloca (yystacksize * sizeof (*yyssp));
        __yy_bcopy ((char *)yyss1, (char *)yyss, size * sizeof (*yyssp));
        yyvs = (YYSTYPE *) alloca (yystacksize * sizeof (*yyvsp));
        __yy_bcopy ((char *)yyvs1, (char *)yyvs, size * sizeof (*yyvsp));
#ifdef YYLSP_NEEDED
        yyls = (YYLTYPE *) alloca (yystacksize * sizeof (*yylsp));
        __yy_bcopy ((char *)yyls1, (char *)yyls, size * sizeof (*yylsp));
#endif
#endif /* no yyoverflow */

        yyssp = yyss + size - 1;
        yyvsp = yyvs + size - 1;
#ifdef YYLSP_NEEDED
        yylsp = yyls + size - 1;
#endif

#if YYDEBUG != 0
        if (yydebug)
            fprintf(stderr, "Stack size increased to %d\n", yystacksize);
#endif

        if (yyssp >= yyss + yystacksize - 1)
            YYABORT;
    }

#if YYDEBUG != 0
    if (yydebug)
        fprintf(stderr, "Entering state %d\n", yystate);
#endif

    goto yybackup;
yybackup:

    /* Do appropriate processing given the current state.  */
    /* Read a lookahead token if we need one and don't already have one.  */
    /* yyresume: */

    /* First try to decide what to do without reference to lookahead token.  */

    yyn = yypact[yystate];
    if (yyn == YYFLAG)
        goto yydefault;

    /* Not known => get a lookahead token if don't already have one.  */

    /* yychar is either YYEMPTY or YYEOF
       or a valid token in external form.  */

    if (yychar == YYEMPTY) {
#if YYDEBUG != 0
        if (yydebug)
            fprintf(stderr, "Reading a token: ");
#endif
        yychar = YYLEX;
    }

    /* Convert token to internal form (in yychar1) for indexing tables with */

    if (yychar <= 0) {	/* This means end of input. */
        yychar1 = 0;
        yychar = YYEOF;		/* Don't call YYLEX any more */

#if YYDEBUG != 0
        if (yydebug)
            fprintf(stderr, "Now at end of input.\n");
#endif
    } else {
        yychar1 = YYTRANSLATE(yychar);

#if YYDEBUG != 0
        if (yydebug) {
            fprintf (stderr, "Next token is %d (%s", yychar, yytname[yychar1]);
            /* Give the individual parser a way to print the precise meaning
               of a token, for further debugging info.  */
#ifdef YYPRINT
            YYPRINT (stderr, yychar, yylval);
#endif
            fprintf (stderr, ")\n");
        }
#endif
    }

    yyn += yychar1;
    if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != yychar1)
        goto yydefault;

    yyn = yytable[yyn];

    /* yyn is what to do for this token type in this state.
       Negative => reduce, -yyn is rule number.
       Positive => shift, yyn is new state.
         New state is final state => don't bother to shift,
         just return success.
       0, or most negative number => error.  */

    if (yyn < 0) {
        if (yyn == YYFLAG)
            goto yyerrlab;
        yyn = -yyn;
        goto yyreduce;
    } else if (yyn == 0)
        goto yyerrlab;

    if (yyn == YYFINAL)
        YYACCEPT;

    /* Shift the lookahead token.  */

#if YYDEBUG != 0
    if (yydebug)
        fprintf(stderr, "Shifting token %d (%s), ", yychar, yytname[yychar1]);
#endif

    /* Discard the token being shifted unless it is eof.  */
    if (yychar != YYEOF)
        yychar = YYEMPTY;

    *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
    *++yylsp = yylloc;
#endif

    /* count tokens shifted since error; after three, turn off error status.  */
    if (yyerrstatus) yyerrstatus--;

    yystate = yyn;
    goto yynewstate;

    /* Do the default action for the current state.  */
yydefault:

    yyn = yydefact[yystate];
    if (yyn == 0)
        goto yyerrlab;

    /* Do a reduction.  yyn is the number of a rule to reduce with.  */
yyreduce:
    yylen = yyr2[yyn];
    yyval = yyvsp[1-yylen]; /* implement default value of the action */

#if YYDEBUG != 0
    if (yydebug) {
        int i;

        fprintf (stderr, "Reducing via rule %d (line %d), ",
                 yyn, yyrline[yyn]);

        /* Print the symbols being reduced, and their result.  */
        for (i = yyprhs[yyn]; yyrhs[i] > 0; i++)
            fprintf (stderr, "%s ", yytname[yyrhs[i]]);
        fprintf (stderr, " -> %s\n", yytname[yyr1[yyn]]);
    }
#endif


    switch (yyn) {

    case 1:
#line 39 "yacc.y"
        {
            createRoot(yyvsp[-6],yyvsp[-4],yyvsp[-2]);;
            break;
        }
    case 2:
#line 40 "yacc.y"
        {
            createRoot(0,0,yyvsp[-1]);;
            break;
        }
    case 3:
#line 44 "yacc.y"
        {
            yyval=node3(variables,yyvsp[-2],yyvsp[0],1,0);;
            break;
        }
    case 4:
#line 45 "yacc.y"
        {
            yyval=node2(variables,yyvsp[0],1);;
            break;
        }
    case 5:
#line 49 "yacc.y"
        {
            yyval=node3(assigns,yyvsp[-1],yyvsp[0],0,0);;
            break;
        }
    case 6:
#line 50 "yacc.y"
        {
            yyval=node2(assigns,yyvsp[-1],0);;
            break;
        }
    case 7:
#line 54 "yacc.y"
        {
            yyval=node3(assignExpr,yyvsp[-2],yyvsp[0],1,0);;
            break;
        }
    case 8:
#line 58 "yacc.y"
        {
            yyval=node3(results,yyvsp[-3],yyvsp[0],0,0);;
            break;
        }
    case 9:
#line 59 "yacc.y"
        {
            yyval=node3(results,yyvsp[-2],yyvsp[0],0,0);;
            break;
        }
    case 10:
#line 60 "yacc.y"
        {
            yyval=node2(results,yyvsp[-1],0);;
            break;
        }
    case 11:
#line 61 "yacc.y"
        {
            yyval=node2(results,yyvsp[0],0);;
            break;
        }
    case 12:
#line 65 "yacc.y"
        {
            yyval=node3(exprs,yyvsp[-2],yyvsp[0],0,0);;
            break;
        }
    case 13:
#line 66 "yacc.y"
        {
            yyval=node2(exprs,yyvsp[0],0);;
            break;
        }
    case 14:
#line 70 "yacc.y"
        {
            yyval=node3(expr,yyvsp[-1],yyvsp[0],1,0);;
            break;
        }
    case 15:
#line 71 "yacc.y"
        {
            yyval=node3(expr,yyvsp[-1],yyvsp[0],1,0);;
            break;
        }
    case 16:
#line 72 "yacc.y"
        {
            yyval=node4(expr,yyvsp[-1],yyvsp[-2],yyvsp[0],1,0,0);;
            break;
        }
    case 17:
#line 73 "yacc.y"
        {
            yyval=node4(expr,yyvsp[-1],yyvsp[-2],yyvsp[0],1,0,0);;
            break;
        }
    case 18:
#line 74 "yacc.y"
        {
            yyval=node2(expr,yyvsp[0],0);;
            break;
        }
    case 19:
#line 78 "yacc.y"
        {
            yyval=node4(term,yyvsp[-1],yyvsp[-2],yyvsp[0],1,0,0);;
            break;
        }
    case 20:
#line 79 "yacc.y"
        {
            yyval=node4(term,yyvsp[-1],yyvsp[-2],yyvsp[0],1,0,0);;
            break;
        }
    case 21:
#line 80 "yacc.y"
        {
            yyval=node2(term,yyvsp[0],0);;
            break;
        }
    case 22:
#line 84 "yacc.y"
        {
            yyval=node4(factor,yyvsp[-1],yyvsp[-2],yyvsp[0],1,0,0);;
            break;
        }
    case 23:
#line 85 "yacc.y"
        {
            yyval=node2(factor,yyvsp[0],0);;
            break;
        }
    case 24:
#line 89 "yacc.y"
        {
            yyval=node2(bracket,yyvsp[-1],0);;
            break;
        }
    case 25:
#line 90 "yacc.y"
        {
            yyval=node3(func,yyvsp[-3],yyvsp[-1],1,0);;
            break;
        }
    case 26:
#line 91 "yacc.y"
        {
            yyval=node2(param,yyvsp[0],1);;
            break;
        }
    case 27:
#line 92 "yacc.y"
        {
            yyval=node4(diff,yyvsp[-5],yyvsp[-3],yyvsp[-1],1,1,1);;
            break;
        }
    case 28:
#line 93 "yacc.y"
        {
            yyval=node2(adouble,yyvsp[0],1);;
            break;
        }
    case 29:
#line 94 "yacc.y"
        {
            yyval=node2(aint,yyvsp[0],1);;
            break;
        }
    case 30:
#line 95 "yacc.y"
        {
            yyval=node2(avar,yyvsp[0],1);;
            break;
        }
    }
    /* the action file gets copied in in place of this dollarsign */
#line 442 "bison.simple"
    
    yyvsp -= yylen;
    yyssp -= yylen;
#ifdef YYLSP_NEEDED
    yylsp -= yylen;
#endif

#if YYDEBUG != 0
    if (yydebug) {
        short *ssp1 = yyss - 1;
        fprintf (stderr, "state stack now");
        while (ssp1 != yyssp)
            fprintf (stderr, " %d", *++ssp1);
        fprintf (stderr, "\n");
    }
#endif

    *++yyvsp = yyval;

#ifdef YYLSP_NEEDED
    yylsp++;
    if (yylen == 0) {
        yylsp->first_line = yylloc.first_line;
        yylsp->first_column = yylloc.first_column;
        yylsp->last_line = (yylsp-1)->last_line;
        yylsp->last_column = (yylsp-1)->last_column;
        yylsp->text = 0;
    } else {
        yylsp->last_line = (yylsp+yylen-1)->last_line;
        yylsp->last_column = (yylsp+yylen-1)->last_column;
    }
#endif

    /* Now "shift" the result of the reduction.
       Determine what state that goes to,
       based on the state we popped back to
       and the rule number reduced by.  */

    yyn = yyr1[yyn];

    yystate = yypgoto[yyn - YYNTBASE] + *yyssp;
    if (yystate >= 0 && yystate <= YYLAST && yycheck[yystate] == *yyssp)
        yystate = yytable[yystate];
    else
        yystate = yydefgoto[yyn - YYNTBASE];

    goto yynewstate;

yyerrlab:   /* here on detecting error */

    if (! yyerrstatus)
        /* If not already recovering from an error, report this error.  */
    {
        ++yynerrs;

#ifdef YYERROR_VERBOSE
        yyn = yypact[yystate];

        if (yyn > YYFLAG && yyn < YYLAST) {
            int size = 0;
            char *msg;
            int x, count;

            count = 0;
            for (x = 0; x < (sizeof(yytname) / sizeof(char *)); x++)
                if (yycheck[x + yyn] == x)
                    size += strlen(yytname[x]) + 15, count++;
            msg = (char *) malloc(size + 15);
            if (msg != 0) {
                strcpy(msg, "parse error");

                if (count < 5) {
                    count = 0;
                    for (x = 0; x < (sizeof(yytname) / sizeof(char *)); x++)
                        if (yycheck[x + yyn] == x) {
                            strcat(msg, count == 0 ? ", expecting `" : " or `");
                            strcat(msg, yytname[x]);
                            strcat(msg, "'");
                            count++;
                        }
                }
                yyerror(msg);
                free(msg);
            } else
                yyerror ("parse error; also virtual memory exceeded");
        } else
#endif /* YYERROR_VERBOSE */
            yyerror("parse error");
    }

    goto yyerrlab1;
yyerrlab1:   /* here on error raised explicitly by an action */

    if (yyerrstatus == 3) {
        /* if just tried and failed to reuse lookahead token after an error, discard it.  */

        /* return failure if at end of input */
        if (yychar == YYEOF)
            YYABORT;

#if YYDEBUG != 0
        if (yydebug)
            fprintf(stderr, "Discarding token %d (%s).\n", yychar, yytname[yychar1]);
#endif

        yychar = YYEMPTY;
    }

    /* Else will try to reuse lookahead token
       after shifting the error token.  */

    yyerrstatus = 3;		/* Each real token shifted decrements this */

    goto yyerrhandle;

yyerrdefault:  /* current state does not do anything special for the error token. */

#if 0
    /* This is wrong; only states that explicitly want error tokens
       should shift them.  */
    yyn = yydefact[yystate];  /* If its default is to accept any token, ok.  Otherwise pop it.*/
    if (yyn) goto yydefault;
#endif

yyerrpop:   /* pop the current state because it cannot handle the error token */

    if (yyssp == yyss) YYABORT;
    yyvsp--;
    yystate = *--yyssp;
#ifdef YYLSP_NEEDED
    yylsp--;
#endif

#if YYDEBUG != 0
    if (yydebug) {
        short *ssp1 = yyss - 1;
        fprintf (stderr, "Error: state stack now");
        while (ssp1 != yyssp)
            fprintf (stderr, " %d", *++ssp1);
        fprintf (stderr, "\n");
    }
#endif

yyerrhandle:

    yyn = yypact[yystate];
    if (yyn == YYFLAG)
        goto yyerrdefault;

    yyn += YYTERROR;
    if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != YYTERROR)
        goto yyerrdefault;

    yyn = yytable[yyn];
    if (yyn < 0) {
        if (yyn == YYFLAG)
            goto yyerrpop;
        yyn = -yyn;
        goto yyreduce;
    } else if (yyn == 0)
        goto yyerrpop;

    if (yyn == YYFINAL)
        YYACCEPT;

#if YYDEBUG != 0
    if (yydebug)
        fprintf(stderr, "Shifting error token, ");
#endif

    *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
    *++yylsp = yylloc;
#endif

    yystate = yyn;
    goto yynewstate;
}
#line 97 "yacc.y"


void yyerror(char *s)
{
    fprintf(stderr, "%s\n", s);
}

tree* root=0;
void* createRoot(void* a,void* b,void* c)
{
    tree* ret=(tree*)malloc(sizeof(tree));
    ret->type=program;
    ret->a=a;
    ret->b=b;
    ret->c=c;
    ret->ta=0;
    ret->tb=0;
    ret->tc=0;
    root=ret;
    return ret;
}
void* node2(enum treetype name,void* a,int ta)
{
    tree* ret=(tree*)malloc(sizeof(tree));
    ret->type=name;
    ret->a=a;
    ret->b=0;
    ret->c=0;
    ret->ta=ta;
    ret->tb=-1;
    ret->tc=-1;
    return ret;
}
void* node3(enum treetype name,void* a,void* b,int ta,int tb)
{
    tree* ret=(tree*)malloc(sizeof(tree));
    ret->type=name;
    ret->a=a;
    ret->b=b;
    ret->c=0;
    ret->ta=ta;
    ret->tb=tb;
    ret->tc=-1;
    return ret;
}
void* node4(enum treetype name,void* a,void* b,void* c,int ta,int tb,int tc)
{
    tree* ret=(tree*)malloc(sizeof(tree));
    ret->type=name;
    ret->a=a;
    ret->b=b;
    ret->c=c;
    ret->ta=ta;
    ret->tb=tb;
    ret->tc=tc;
    return ret;
}