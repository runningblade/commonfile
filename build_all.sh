#!/bin/bash
echo "Build using $1 threads!"

if [ ! -d "../commonfile-build" ]; then
  mkdir ../commonfile-build
fi
cd ../commonfile-build

if [ ! -d "DebugF" ]; then
  mkdir DebugF
fi
cd DebugF
cmake ../../commonfile -DCMAKE_BUILD_TYPE=Debug
make -j $1
cd ..

if [ ! -d "DebugD" ]; then
  mkdir DebugD
fi
cd DebugD
cmake ../../commonfile -DDOUBLE_PRECISION=1 -DCMAKE_BUILD_TYPE=Debug
make -j $1
cd ..

if [ ! -d "DebugQ" ]; then
  mkdir DebugQ
fi
cd DebugQ
cmake ../../commonfile -DUSE_QUADMATH=1 -DCMAKE_BUILD_TYPE=Debug -DNO_TOOLS=1
make -j $1
cd ..

if [ ! -d "ReleaseF" ]; then
  mkdir ReleaseF
fi
cd ReleaseF
cmake ../../commonfile -DCMAKE_BUILD_TYPE=Release
make -j $1
cd ..

if [ ! -d "ReleaseD" ]; then
  mkdir ReleaseD
fi
cd ReleaseD
cmake ../../commonfile -DDOUBLE_PRECISION=1 -DCMAKE_BUILD_TYPE=Release
make -j $1
cd ..

if [ ! -d "ReleaseQ" ]; then
  mkdir ReleaseQ
fi
cd ReleaseQ
cmake ../../commonfile -DUSE_QUADMATH=1 -DCMAKE_BUILD_TYPE=Release -DNO_TOOLS=1
make -j $1
cd ..

if [ ! -d "MaximaToCpp-build" ]; then
  mkdir MaximaToCpp-build
fi
cd MaximaToCpp-build
cmake ../../commonfile/MaximaToCpp
make -j $1
cd ..

