#include "Hash.h"

USE_PRJ_NAMESPACE

size_t Hash::operator()(const Vec2i& key) const
{
  size_t seed=0;
  hash_combine(seed,std::hash<sizeType>()(key[0]));
  hash_combine(seed,std::hash<sizeType>()(key[1]));
  return seed;
}
size_t Hash::operator()(const Vec3i& key) const
{
  size_t seed=0;
  hash_combine(seed,std::hash<sizeType>()(key[0]));
  hash_combine(seed,std::hash<sizeType>()(key[1]));
  hash_combine(seed,std::hash<sizeType>()(key[2]));
  return seed;
}
size_t Hash::operator()(const Vec4i& key) const
{
  size_t seed=0;
  hash_combine(seed,std::hash<sizeType>()(key[0]));
  hash_combine(seed,std::hash<sizeType>()(key[1]));
  hash_combine(seed,std::hash<sizeType>()(key[2]));
  hash_combine(seed,std::hash<sizeType>()(key[3]));
  return seed;
}
size_t Hash::operator()(const std::pair<Vec2i,Vec2i>& key) const
{
  size_t seed=0;
  hash_combine(seed,std::hash<sizeType>()(key.first[0]));
  hash_combine(seed,std::hash<sizeType>()(key.first[1]));
  hash_combine(seed,std::hash<sizeType>()(key.second[0]));
  hash_combine(seed,std::hash<sizeType>()(key.second[1]));
  return seed;
}
size_t Hash::operator()(const std::pair<Vec3i,Vec3i>& key) const
{
  size_t seed=0;
  hash_combine(seed,std::hash<sizeType>()(key.first[0]));
  hash_combine(seed,std::hash<sizeType>()(key.first[1]));
  hash_combine(seed,std::hash<sizeType>()(key.first[2]));
  hash_combine(seed,std::hash<sizeType>()(key.second[0]));
  hash_combine(seed,std::hash<sizeType>()(key.second[1]));
  hash_combine(seed,std::hash<sizeType>()(key.second[2]));
  return seed;
}
size_t Hash::operator()(const std::pair<Vec4i,Vec4i>& key) const
{
  size_t seed=0;
  hash_combine(seed,std::hash<sizeType>()(key.first[0]));
  hash_combine(seed,std::hash<sizeType>()(key.first[1]));
  hash_combine(seed,std::hash<sizeType>()(key.first[2]));
  hash_combine(seed,std::hash<sizeType>()(key.first[3]));
  hash_combine(seed,std::hash<sizeType>()(key.second[0]));
  hash_combine(seed,std::hash<sizeType>()(key.second[1]));
  hash_combine(seed,std::hash<sizeType>()(key.second[2]));
  hash_combine(seed,std::hash<sizeType>()(key.second[3]));
  return seed;
}
