#ifndef HASH_H
#define HASH_H

#include <CommonFile/MathBasic.h>

PRJ_BEGIN

FORCE_INLINE void hash_combine(std::size_t& seed,size_t v)
{
  seed^=v+0x9e3779b9+(seed<<6)+(seed>>2);
}
struct Hash
{
  size_t operator()(const Vec2i& key) const;
  size_t operator()(const Vec3i& key) const;
  size_t operator()(const Vec4i& key) const;
  size_t operator()(const std::pair<Vec2i,Vec2i>& key) const;
  size_t operator()(const std::pair<Vec3i,Vec3i>& key) const;
  size_t operator()(const std::pair<Vec4i,Vec4i>& key) const;
};

PRJ_END

#endif
