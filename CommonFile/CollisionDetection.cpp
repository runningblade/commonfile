#include "CollisionDetectionImpl.h"

PRJ_BEGIN

//this is a workaround for fast gauss transform used in GraspPlanner
#ifdef ALL_TYPES
template struct BBox<__float128,2>;
template struct BBox<__float128,3>;
template struct BBox<mpfr::mpreal,2>;
template struct BBox<mpfr::mpreal,3>;
template class TriangleTpl<__float128>;
template class TriangleTpl<mpfr::mpreal>;
template class TetrahedronTpl<__float128>;
template class TetrahedronTpl<mpfr::mpreal>;
template class LineSegTpl<__float128>;
template class LineSegTpl<mpfr::mpreal>;
template class Sphere<__float128>;
template class Sphere<mpfr::mpreal>;
#endif

template struct BBox<scalarD,2>;
template struct BBox<scalarD,3>;
template class LineSegTpl<scalarD>;
template class PlaneTpl<scalarD>;
template class TriangleTpl<scalarD>;
template class TetrahedronTpl<scalarD>;
template class OBBTpl<scalarD,2>;
template class OBBTpl<scalarD,3>;
template class KDOP18<scalarD>;
template class Sphere<scalarD>;

template struct BBox<scalarF,2>;
template struct BBox<scalarF,3>;
template class LineSegTpl<scalarF>;
template class PlaneTpl<scalarF>;
template class TriangleTpl<scalarF>;
template class TetrahedronTpl<scalarF>;
template class OBBTpl<scalarF,2>;
template class OBBTpl<scalarF,3>;
template class KDOP18<scalarF>;
template class Sphere<scalarF>;

template struct BBox<sizeType,2>;
template struct BBox<sizeType,3>;
template class LineSegTpl<sizeType>;
template class PlaneTpl<sizeType>;
template class TriangleTpl<sizeType>;
template class TetrahedronTpl<sizeType>;
template class OBBTpl<sizeType,2>;
template class OBBTpl<sizeType,3>;
template class KDOP18<sizeType>;
template class Sphere<sizeType>;

template struct BBox<char,2>;
template struct BBox<char,3>;
template class LineSegTpl<char>;
template class PlaneTpl<char>;
template class TriangleTpl<char>;
template class TetrahedronTpl<char>;
template class OBBTpl<char,2>;
template class OBBTpl<char,3>;
template class KDOP18<char>;
template class Sphere<char>;

template struct BBox<unsigned char,2>;
template struct BBox<unsigned char,3>;
template class LineSegTpl<unsigned char>;
template class PlaneTpl<unsigned char>;
template class TriangleTpl<unsigned char>;
template class TetrahedronTpl<unsigned char>;
template class OBBTpl<unsigned char,2>;
template class OBBTpl<unsigned char,3>;
template class KDOP18<unsigned char>;
template class Sphere<unsigned char>;

PRJ_END
