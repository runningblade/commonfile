#include <CommonFile/IO.h>
#include <CommonFile/DisjointSet.h>
#include <experimental/filesystem>

USE_PRJ_NAMESPACE

template <typename T>
void testIO(T v)
{
  std::shared_ptr<IOData> dat=getIOData();
  {
    std::ofstream os("test.dat",std::ios::binary);
    writeBinaryData(v,os,dat.get());
  }
  {
    std::ifstream is("test.dat",std::ios::binary);
    readBinaryData(v,is,dat.get());
  }
}
int main(int argc,char** argv)
{
  {
    std::pair<int,int> v;
    testIO(v);
  }
  {
    std::tuple<int,int,int> v;
    testIO(v);
  }
  {
    std::vector<int> v;
    for(sizeType i=0; i<100; i++)
      v.push_back(RandEngine::randI(0,1000));
    testIO(v);
  }
  {
    std::deque<int> v;
    for(sizeType i=0; i<100; i++)
      v.push_back(RandEngine::randI(0,1000));
    testIO(v);
  }
  {
    std::list<int> v;
    for(sizeType i=0; i<100; i++)
      v.push_back(RandEngine::randI(0,1000));
    testIO(v);
  }
  {
    std::set<int> v;
    for(sizeType i=0; i<100; i++)
      v.insert(RandEngine::randI(0,1000));
    testIO(v);
  }
  {
    std::map<int,float> v;
    for(sizeType i=0; i<100; i++)
      v[RandEngine::randI(0,1000)]=RandEngine::randR(0,1);
    testIO(v);
  }
  {
    std::unordered_set<int> v;
    for(sizeType i=0; i<100; i++)
      v.insert(RandEngine::randI(0,1000));
    testIO(v);
  }
  {
    std::unordered_map<int,float> v;
    for(sizeType i=0; i<100; i++)
      v[RandEngine::randI(0,1000)]=RandEngine::randR(0,1);
    testIO(v);
  }
  {
    DisjointSet<int> dset(100);
    testIO(dset);
  }
  return 0;
}
