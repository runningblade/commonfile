echo Build using %1 threads!
@echo off

REM install cmake (at least version 3.10)
REM add this path to your system path: C:\Program Files\CMake\bin
REM clone commonfile by running: git clone https://runningblade@bitbucket.org/runningblade/commonfile.git
REM cd into commonfile
REM run build_all.bat

if not exist %~dp0\..\commonfile-build (
	mkdir %~dp0\..\commonfile-build
)
cd %~dp0\..\commonfile-build
cd %~dp0

if not exist %~dp0\..\commonfile-build\DebugF (
	mkdir %~dp0\..\commonfile-build\DebugF
)
cd %~dp0\..\commonfile-build\DebugF
cmake ..\..\commonfile -DCMAKE_BUILD_TYPE=Debug -G "Visual Studio 16 2019" -A  x64
msbuild ALL_BUILD.vcxproj /property:Configuration=Debug -maxcpucount:%1
cd %~dp0

if not exist %~dp0\..\commonfile-build\DebugD (
	mkdir %~dp0\..\commonfile-build\DebugD
)
cd %~dp0\..\commonfile-build\DebugD
cmake ..\..\commonfile -DDOUBLE_PRECISION=1 -DCMAKE_BUILD_TYPE=Debug -G "Visual Studio 16 2019" -A  x64
msbuild ALL_BUILD.vcxproj /property:Configuration=Debug -maxcpucount:%1
cd %~dp0

if not exist %~dp0\..\commonfile-build\ReleaseF (
	mkdir %~dp0\..\commonfile-build\ReleaseF
)
cd %~dp0\..\commonfile-build\ReleaseF
cmake ..\..\commonfile -DCMAKE_BUILD_TYPE=Release -G "Visual Studio 16 2019" -A  x64
msbuild ALL_BUILD.vcxproj /property:Configuration=Release -maxcpucount:%1
cd %~dp0

if not exist %~dp0\..\commonfile-build\ReleaseD (
	mkdir %~dp0\..\commonfile-build\ReleaseD
)
cd %~dp0\..\commonfile-build\ReleaseD
cmake ..\..\commonfile -DDOUBLE_PRECISION=1 -DCMAKE_BUILD_TYPE=Release -G "Visual Studio 16 2019" -A  x64
msbuild ALL_BUILD.vcxproj /property:Configuration=Release -maxcpucount:%1
cd %~dp0
