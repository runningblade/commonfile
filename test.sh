#!/bin/bash
cp -avr $(pwd)/data/* $(pwd)/../commonfile-build/ReleaseD/
cd ../commonfile-build/ReleaseD/
./mainDebugCollisionDetection
./mainDebugDynamicBVH
./mainDebugGeomCell
./mainDebugIO
./mainDebugMarchingCube
./mainDebugRadixSort 100
./mainObjMeshGetKRing bunny.obj 4
./mainObjMeshToSDF bunny.obj 2.5 512 10

