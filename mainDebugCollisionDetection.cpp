#include <CommonFile/CollisionDetection.h>
#include <CommonFile/IO.h>
#include <experimental/filesystem>

USE_PRJ_NAMESPACE

void writePtVTK(const Vec3d& pt,VTKWriter<scalarD>& os)
{
  std::vector<Vec3d,Eigen::aligned_allocator<Vec3d>> pts(1,pt);
  std::vector<Vec3i,Eigen::aligned_allocator<Vec3i>> iss(1,Vec3i::Constant(0));
  os.setRelativeIndex();
  os.appendPoints(pts.begin(),pts.end());
  os.appendCells(iss.begin(),iss.end(),VTKWriter<scalarD>::POINT,true);
}
void writeBBVTK(const BBox<scalarD>& pt,VTKWriter<scalarD>& os)
{
  std::vector<Vec3d,Eigen::aligned_allocator<Vec3d>> pts;
  pts.push_back(pt._minC);
  pts.push_back(pt._maxC);
  os.setRelativeIndex();
  os.appendVoxels(pts.begin(),pts.end(),true,true);
}
int main(int argc,char** argv)
{
  if(std::experimental::filesystem::v1::exists("Coll"))
    std::experimental::filesystem::v1::remove_all("Coll");
  std::experimental::filesystem::v1::create_directories("Coll");
  for(sizeType i=0,j=0,k=0; i<1000; i++) {
    Mat3d MASK3=Mat3d::Identity();
    MASK3(2,2)=0;
    scalarD t;
    LineSegTpl<scalarD> a(MASK3*Vec3d::Random(),MASK3*Vec3d::Random());
    LineSegTpl<scalarD> b(MASK3*Vec3d::Random(),MASK3*Vec3d::Random());
    if(a.intersect(b,t,false)) {
      Vec3d p2=b._x*(1-t)+b._y*t;
      VTKWriter<scalarD> os("LineSeg","Coll/LineSeg"+std::to_string(j++)+".vtk",true);
      a.writeVTK(os);
      b.writeVTK(os);
      writePtVTK(p2,os);
    }
    if(a.intersect(b,t,true)) {
      Vec3d p2=b._x*(1-t)+b._y*t;
      VTKWriter<scalarD> os("LineSeg","Coll/Line"+std::to_string(k++)+".vtk",true);
      a.writeVTK(os);
      b.writeVTK(os);
      writePtVTK(p2,os);
    }
  }
  for(sizeType i=0,j=0,k=0; i<1000; i++) {
    scalarD sqrDistance;
    LineSegTpl<scalarD> a(Vec3d::Random(),Vec3d::Random());
    LineSegTpl<scalarD> b(Vec3d::Random(),Vec3d::Random());
    Vec3d c=Vec3d::Random(),cp,B;
    a.calcPointDist(c,sqrDistance,cp,B);
    {
      VTKWriter<scalarD> os("LineSeg","Coll/LineSeg2Point"+std::to_string(j++)+".vtk",true);
      LineSegTpl<scalarD> dist(c,a._x*B[0]+a._y*B[1]);
      ASSERT((dist.length()-std::sqrt(sqrDistance))<1E-3f)
      a.writeVTK(os);
      dist.writeVTK(os);
      writePtVTK(c,os);
    }
    {
      scalarD t1,t2;
      a.calcLineDist(b,sqrDistance,t1,t2);
      VTKWriter<scalarD> os("LineSeg","Coll/LineSeg2LineSeg"+std::to_string(k++)+".vtk",true);
      LineSegTpl<scalarD> dist(a._x*(1-t1)+a._y*t1,b._x*(1-t2)+b._y*t2);
      ASSERT((dist.length()-std::sqrt(sqrDistance))<1E-3f)
      a.writeVTK(os);
      b.writeVTK(os);
      dist.writeVTK(os);
    }
  }
  for(sizeType i=0,j=0,k=0; i<1000; i++) {
    Vec3d a=Vec3d::Random(),b=Vec3d::Random();
    TriangleTpl<scalarD> t(Vec3d::Random(),Vec3d::Random(),Vec3d::Random());
    BBox<scalarD> bb(Vec3d(std::min(a[0],b[0]),std::min(a[1],b[1]),std::min(a[2],b[2])),
                     Vec3d(std::max(a[0],b[0]),std::max(a[1],b[1]),std::max(a[2],b[2])));
    if(t.intersect(bb)) {
      VTKWriter<scalarD> os("LineSeg","Coll/Triangle2BBox"+std::to_string(j++)+".vtk",true);
      t.writeVTK(os);
      writeBBVTK(bb,os);
    } else {
      VTKWriter<scalarD> os("LineSeg","Coll/Triangle2BBoxNoColl"+std::to_string(k++)+".vtk",true);
      t.writeVTK(os);
      writeBBVTK(bb,os);
    }
  }
  for(sizeType i=0,j=0; i<1000; i++) {
    Vec3d bt;
    Vec2d bl;
    scalarD sqrDistance;
    TriangleTpl<scalarD> a(Vec3d::Random(),Vec3d::Random(),Vec3d::Random());
    LineSegTpl<scalarD> l(Vec3d::Random(),Vec3d::Random());
    if(!a.calcLineDist(l,sqrDistance,bt,bl)) {
      VTKWriter<scalarD> os("LineSeg","Coll/Triangle2LineSegDist"+std::to_string(j++)+".vtk",true);
      LineSegTpl<scalarD> dist(a._a*bt[0]+a._b*bt[1]+a._c*bt[2],l._x*bl[0]+l._y*bl[1]);
      ASSERT((dist.length()-std::sqrt(sqrDistance))<1E-3f)
      dist.writeVTK(os);
      a.writeVTK(os);
      l.writeVTK(os);
    }
  }
  for(sizeType i=0,j=0; i<1000; i++) {
    Vec3d b1,b2;
    scalarD sqrDistance;
    TriangleTpl<scalarD> a(Vec3d::Random(),Vec3d::Random(),Vec3d::Random());
    TriangleTpl<scalarD> b(Vec3d::Random(),Vec3d::Random(),Vec3d::Random());
    if(!a.calcTriangleDist(b,sqrDistance,b1,b2)) {
      VTKWriter<scalarD> os("LineSeg","Coll/Triangle2Triangle"+std::to_string(j++)+".vtk",true);
      LineSegTpl<scalarD> dist(a._a*b1[0]+a._b*b1[1]+a._c*b1[2],b._a*b2[0]+b._b*b2[1]+b._c*b2[2]);
      ASSERT((dist.length()-std::sqrt(sqrDistance))<1E-3f)
      dist.writeVTK(os);
      a.writeVTK(os);
      b.writeVTK(os);
    }
  }
  return 0;
}
