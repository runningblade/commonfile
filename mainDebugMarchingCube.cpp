#include <CommonFile/MarchingCube2D.h>
#include <CommonFile/MarchingCube2DVolume.h>
#include <CommonFile/MarchingCube3D.h>

USE_PRJ_NAMESPACE

void mc2D()
{
  BBox<scalar> bb;
  bb._minC=Vec3(-1,-1,0);
  bb._maxC=Vec3( 1, 1,0);
  ScalarField sphere2D;
  sphere2D.reset(Vec3i(64,64,0),bb,0);
  for(sizeType x=0; x<sphere2D.getNrPoint()[0]; x++)
    for(sizeType y=0; y<sphere2D.getNrPoint()[0]; y++)
      sphere2D.get(Vec3i(x,y,0))=sphere2D.getPt(Vec3i(x,y,0)).norm()-0.5;
  ObjMesh mesh;

  MarchingCube2D<scalar> mc2D(mesh);
  mc2D.solve(sphere2D,0);
  mesh.writeVTK("sphere2D.vtk",true);

  MarchingCube2DVolume<scalar> mc2DV(mesh);
  mc2DV.solve(sphere2D,0);
  mesh.writeVTK("sphere2DVolume.vtk",true);
}
void mc3D()
{
  BBox<scalar> bb;
  bb._minC.setConstant(-1);
  bb._maxC.setConstant( 1);
  ScalarField sphere3D;
  sphere3D.reset(Vec3i::Constant(64),bb,0);
  for(sizeType x=0; x<sphere3D.getNrPoint()[0]; x++)
    for(sizeType y=0; y<sphere3D.getNrPoint()[0]; y++)
      for(sizeType z=0; z<sphere3D.getNrPoint()[0]; z++)
        sphere3D.get(Vec3i(x,y,z))=sphere3D.getPt(Vec3i(x,y,z)).norm()-0.5;
  ObjMesh mesh;
  MarchingCube3D<scalar> mc3D(mesh);
  mc3D.solve(sphere3D,0);
  mesh.writeVTK("sphere3D.vtk",true);
}
int main()
{
  mc2D();
  mc3D();
  return 0;
}
