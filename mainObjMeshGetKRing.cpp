#include <CommonFile/ObjMesh.h>
#include <fstream>

USE_PRJ_NAMESPACE

int main(int argc,char** argv)
{
  if(argc != 3) {
    INFO("Usage: mainObjMeshGetKRing [pathObj] [idRing]")
    return -1;
  } else {
    //compute
    ObjMesh mesh;
    sizeType K=atoi(argv[2]),maxN=0;
    std::ifstream is(argv[1]);
    std::vector<std::map<int,int> > KRing;
    mesh.read(is,false,false);
    mesh.smooth();
    mesh.buildKRingV(KRing,K);
    std::vector<std::vector<int> > ret(mesh.getV().size());
    for(sizeType i=0; i<(sizeType)KRing.size(); i++) {
      const std::map<int,int>& KRingVI=KRing[i];
      for(std::map<int,int>::const_iterator beg=KRingVI.begin(),end=KRingVI.end(); beg!=end; beg++)
        if(beg->second == K-1)
          ret[i].push_back(beg->first);
      maxN=std::max<sizeType>(maxN,ret[i].size());
    }
    //write
    Mati retM;
    retM.setZero(ret.size(),maxN);
    for(sizeType i=0; i<(sizeType)ret.size(); i++)
      for(sizeType j=0; j<(sizeType)ret[i].size(); j++)
        retM(i,j)=ret[i][j]+1;
    //write
    std::string path=std::string(argv[1])+"_"+std::string(argv[2])+"Ring.dat";
    std::ofstream os(path);
    INFOV("We output a matrix of size: %ldX%ld, for the ObjMesh with %ld vertices and a maximum of %ld %ld-ring neighbors!",retM.rows(),retM.cols(),retM.rows(),retM.cols(),K)
    INFOV("Output path: %s!",path.c_str())
    os << retM;
  }
  return 0;
}
