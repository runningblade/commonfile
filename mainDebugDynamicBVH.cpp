#include <CommonFile/geom/StaticGeom.h>

USE_PRJ_NAMESPACE

void update(StaticGeom& geom)
{
  bool updated=geom.update();
  INFOV("updated: %s, depth: %ld, nrBall: %ld!",std::to_string(updated).c_str(),geom.depth(),geom.nrG())
  geom.parityCheck();
}
void randomAdd(StaticGeom& geom,sizeType nrBall)
{
  INFO("Randomly add balls!")
  for(sizeType i=0; i<nrBall; i++) {
    geom.addGeomSphere(Vec3::Random(),RandEngine::randR(0.01f,0.1f));
    geom.parityCheck();
    update(geom);
  }
}
void randomPerturb(StaticGeom& geom)
{
  INFO("Randomly perturb balls!")
  Mat4 T=Mat4::Identity();
  for(sizeType i=0; i<geom.nrG(); i++) {
    T.block<3,1>(0,3)=Vec3::Random();
    geom.getG(i).setT(T);
    update(geom);
  }
}
void randomRemove(StaticGeom& geom,sizeType nrBall)
{
  INFO("Randomly remove balls!")
  while(geom.nrG()>nrBall) {
    sizeType id=RandEngine::randI(0,geom.nrG()-1);
    geom.removeGeomCell(geom.getGPtr(id));
    geom.parityCheck();
    update(geom);
  }
}
int main(int argc,char** argv)
{
  StaticGeom geom(3);
  for(sizeType i=0;i<10;i++) {
    //random add ball
    randomAdd(geom,1000);
    //random perturb ball
    randomPerturb(geom);
    //random remove ball
    randomRemove(geom,500);
  }
  return 0;
}
