#include <CommonFile/ParticleCD.h>

USE_PRJ_NAMESPACE

int main(int argc,char** argv)
{
  if(argc != 2) {
    INFO("Usage: mainDebugRadixSort [nrArr]")
    return -1;
  } else {
    //random array
    std::vector<sizeType> pid,index;
    for(sizeType i=0; i<atoi(argv[1]); i++)
      pid.push_back(i);
    random_shuffle(pid.begin(),pid.end());
    index.resize(atoi(argv[1]));
    //debug
    std::cout << "Before Sort: ";
    for(sizeType i=0; i<atoi(argv[1]); i++)
      std::cout << pid[i] << " ";
    std::cout << std::endl << std::endl;
    std::vector<sizeType> pidBK=pid,indexBK=index;
    CollisionInterfaceDirect::radixSort(pid.size(),&pid[0],&pidBK[0],&index[0],&indexBK[0]);
    std::cout << "After Sort: ";
    for(sizeType i=0; i<atoi(argv[1]); i++) {
      ASSERT(i==0 || pid[i]>=pid[i-1])
      std::cout << pid[i] << " ";
    }
    std::cout << std::endl << std::endl;
    return 0;
  }
}
